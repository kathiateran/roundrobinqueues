package roundRobin;

import java.util.Scanner;

public class SimulationRRCLI 
{
  public static void main(String[] args)
  {
    Scanner conIn = new Scanner(System.in);

    int minST;     // minimum service time
    int maxST;     // maximum service time
    int numQueues; // number of queues
    int numProcess;   // number of process
    int quantum;    // process quantum
    
    
    String skip;           // skip end of line after reading an integer
    String more = null;    // used to stop or continue processing

    // User Input
    System.out.print("Enter minimum service time: ");
    minST = conIn.nextInt();
    System.out.print("Enter maximum service time: ");
    maxST = conIn.nextInt();
    System.out.print("Enter the Quantum time: ");
    quantum = conIn.nextInt();
    System.out.println();      

    // creates object to perform simulation
    SimulationRR sim = new SimulationRR(minST, maxST, quantum);
   
    do
    {
      // Gets next simulation instance to be processed.
      System.out.print("Enter number of process queues: ");
      numQueues = conIn.nextInt();     
      System.out.print("Enter total number of processes: ");
      numProcess = conIn.nextInt();    
      skip = conIn.nextLine();   
      
      // runs simulation and output average waiting time
      sim.simulate(numQueues, numProcess);
      System.out.println("Average waiting time is " + sim.getAvgWaitTime());
      
      
      // Determine if there is another simulation instance to process
      System.out.println(); 
      System.out.print("Evaluate another simulation instance? (Y=Yes): ");
      more = conIn.nextLine();
    }
    while (more.equalsIgnoreCase("y"));

    System.out.println("Program completed.");
    
   
  }
}