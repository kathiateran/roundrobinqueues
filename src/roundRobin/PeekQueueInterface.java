package roundRobin;

public interface PeekQueueInterface <T> extends QueueInterface<T>
{
	  public T peekFront();
	  // If the queue is empty, returns null.
	  // Otherwise returns the element at the front of this queue.
	  
	  public T peekRear();
	  // If the queue is empty, returns null.
	  // Otherwise returns the element at the rear of this queue.
	}



