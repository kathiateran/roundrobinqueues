package roundRobin;

public class SimulationRR 
{
		  final int MAXTIME = Integer.MAX_VALUE; 
		                               
		  ProcessGenerator procGen;   // a process generator
		  float avgWaitTime = 0.0f;    // average wait time for most recent simulation

		  
		  public SimulationRR(int minST, int maxST, int quantum) 
		  {
		    procGen = new ProcessGenerator(minST, maxST, quantum);
		  }
		  
		  public float getAvgWaitTime()
		  {
		    return avgWaitTime;
		  }

		  public void simulate(int numQueues, int numProcess) 
		  // Preconditions: numQueues > 0
		  //                numCustomers > 0
		  //
		  // Simulates numProcess processes entering and leaving the queue
		  {
		    // the queues
		    LinkedPeekQueue<Process>[] queues = new LinkedPeekQueue[numQueues]; 

		    Process nextProc;      // next process from generator
		    Process proc;          // holds process for temporary use
		    
		    int totWaitTime = 0;    // total wait time
		    int procInCount = 0;    // count of processes started so far
		    int procOutCount = 0;   // count of processes finished so far
		 
		    int nextArrTime;        // next arrival time
		    int nextDepTime;        // next departure time
		    int nextQueue;          // index of queue for next departure
		 
		    int shortest;           // index of shortest queue
		    int shortestSize;       // size of shortest queue
		    Process rearProc;      // process at rear of shortest queue
		    int turnaroundTime;         // calculated finish time for process being enqueued

		    // instantiate the queues
		    for (int i = 0; i < numQueues; i++)
		      queues[i] = new LinkedPeekQueue<Process>();
		 
		    // set process generator and get first process
		    procGen.reset();
		    nextProc = procGen.nextProcess();
		    
		    while (procOutCount < numProcess)  // Processes left to handle
		    { 
		      // get next arrival time
		      if (procInCount != numProcess) 
		        nextArrTime = nextProc.getArrivalTime();
		      else 
		        nextArrTime = MAXTIME; 
		 
		      // get next departure time and set nextQueue
		      nextDepTime = MAXTIME;
		      nextQueue = -1;
		      for (int i = 0; i < numQueues; i++)
		        if (queues[i].size() != 0)
		        {
		          proc = queues[i].peekFront();
		          if (proc.getTurnaroundTime() < nextDepTime)
		          {
		            nextDepTime = proc.getTurnaroundTime();
		            nextQueue = i;
		          }          
		        }

		      if (nextArrTime < nextDepTime)
		      // handle process arriving
		      {
		        // determine shortest queue
		        shortest = 0;
		        shortestSize = queues[0].size();
		        for (int i = 1; i < numQueues; i++)
		        {
		          if (queues[i].size() < shortestSize)
		          {
		            shortest = i;
		            shortestSize = queues[i].size();
		          }
		        }

		        // determine the finish time
		        if (shortestSize == 0)
		        turnaroundTime = nextProc.getArrivalTime() + nextProc.getServiceTime();
		        else
		        {
		          rearProc = queues[shortest].peekRear();
		          turnaroundTime = rearProc.getTurnaroundTime() + nextProc.getServiceTime();
		        }
		    
		        // set finish time and enqueue process
		        nextProc.setTurnaroundTime(turnaroundTime);
		        queues[shortest].enqueue(nextProc);

		        procInCount = procInCount + 1;
		          
		        // if needed, get next Process to enqueue
		        if (procInCount < numProcess)
		          nextProc = procGen.nextProcess();
		      }
		      else
		      // handle Process leaving
		      {
		          proc = queues[nextQueue].dequeue();
		          totWaitTime = totWaitTime + proc.getWaitTime();
		          procOutCount = procOutCount + 1;
		      }
		    }  // end while
		      
		    avgWaitTime = totWaitTime/(float)numProcess;
		   
		  }
}
