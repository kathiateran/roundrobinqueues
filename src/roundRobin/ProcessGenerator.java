package roundRobin;
import java.util.Random;


public class ProcessGenerator 
{
	
	protected int minST; // minimum service time
	protected int maxST; // maximum service time
	protected int quantum; // constant value
	  
	  protected int currTime = 0;   // current time
	  
	  Random rand = new Random();   // random number generator

	  public ProcessGenerator (int minST, int maxST, int quantum)
	  // Preconditions: all arguments >= 0
	  //                minST  <= maxST
	  {
	    this.minST  = minST;     this.maxST  = maxST; 
	    
	    this.quantum = quantum;
	  }

	  public void reset()
	  {
	    currTime = 0;
	  }

	  public Process nextProcess()
	  // Creates and returns the next random process.
	  {
	    int ST;   // next service time	    
	    ST  = minST + rand.nextInt(maxST - minST + 1);
	    
	    currTime = currTime + quantum;   	                        
	    Process next = new Process(currTime, ST);
	    return next;
	  }
	  
	
	
}
