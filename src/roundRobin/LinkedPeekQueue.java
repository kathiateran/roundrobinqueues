package roundRobin;

public class LinkedPeekQueue<T> extends LinkedQueue<T> implements PeekQueueInterface<T>
{
	  public LinkedPeekQueue() 
	  {
	    super();
	  }

	  public T peekFront()
	  // If the queue is empty, returns null.
	  // Otherwise returns the element at the front of this queue.
	  {
	    if (isEmpty())
	       return null;
	    else 
	       return front.getInfo();
	  }
	  
	  public T peekRear()
	  // If the queue is empty, returns null.
	  // Otherwise returns the element at the rear of this queue.
	  {
	    if (isEmpty())
	       return null;
	    else 
	       return rear.getInfo();
	  }
	}