package roundRobin;

public class Process
{

	protected int arrivalTime;
	protected int serviceTime;
	protected int turnaroundTime;

	public Process(int arrivalTime, int serviceTime)
	{
		this.arrivalTime = arrivalTime;
		this.serviceTime = serviceTime;
	}

	public int getArrivalTime()
	{
		return arrivalTime;
	}
		
	public int getServiceTime()
	{
		return serviceTime;
	}
	
	public void setTurnaroundTime(int time)
	{
		turnaroundTime = time;
	}
		
	public int getTurnaroundTime()
	{
		return turnaroundTime;
	}

	public int getWaitTime()
	{
		return (turnaroundTime - arrivalTime - serviceTime);
	}

		}
		 	
	

